---
layout: post
title: Family harvest
featured: false
author: Travis
tags:
  - Photography
  - weddings
image: /images/uploads/family14.jpg
---
Wow! I think it actually works!

You can see the various inputs availiable below and above.

* **Featured:** is a UI element with a star on the home page, it makes featured blog posts distingushable.
* **Tags: ** Tags are used to "tag"  blog posts, this is used for orginization and searching
* **Image:** this is the main hero image that shows up on the home page. (note you can still insert other images into this post area with the + sign at the top of this box)
