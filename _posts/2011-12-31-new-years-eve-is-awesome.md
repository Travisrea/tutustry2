---
layout: post
title: Fall weddings  & Contrasting colors
featured: true
author: Tutu
tags:
  - Photography
  - weddings
  - skills
image: /images/uploads/wedding3.jpg
---


Hi, I’m Mary Gene.

Also known as Tutu (Hawaiian for Grandmother).



I am passionate about photography. Let's take photos of your wedding day, and follow your family through until your children graduate from high school... Nothing makes me happier than capturing special moments for people. From weddings, to seniors, I enjoy all the family times that need to be remembered . Family memories are what I love helping people create. If I can assist you, please contact me.
