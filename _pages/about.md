---
layout: page
title: About
image: /images/uploads/tutusface.jpg
---


Hi, I’m Mary Gene.
Also known as Tutu (Hawaiian for Grandmother)

I am passionate about photography. Let's take photos of your wedding day, and follow your family through until your children graduate from high school... Nothing makes me happier than capturing special moments for people. From weddings, to seniors, I enjoy all the family times that need to be remembered . Family memories are what I love helping people create. If I can assist you, please contact me.


I have always had a camera in my hand. As a child at camp, or on vacation, I was intrigued with capturing images on a camera. Much time has been spent to perfect the technical side of photography, and I am always looking for inspiration. Consulting with clients on location and what to wear is such a fun part of my job. My first live birth session was such a humbling moment for me. The family trusted me enough with such an important time in their lives. Capturing real emotion at that moment was one of my favorite times behind a camera.
