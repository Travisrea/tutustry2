---
layout: page
title: Testimonials
image: /images/uploads/newborn4.jpg
---

>"Marygene has a talent to capture people and families. She is a natural at finding people's joy on camera!! My home is filled with moments she's captured and that we will treasure for a lifetime. Come rain, shine, mud or fussy people...she makes the best out of any photography opportunity and captures brilliant images! She's flexible, kind, funny and a personal friend. Thank you TuTu for all the amazing photos of our family. We adore you!!"

**-Marv & Suzie Colby**




>"I have known MaryGene for over 15 years now. She is very good at what she does. Her eye for composition, flare for capturing moments & creative details evoke emotions with each photo. She has taken many photos for me & my friends. My only regret is I didn't discover her talent sooner. I would recommend her highly to anyone."

**-Tracy Peterson**




>"Tutus Shots has been our personal family photographer for over three years. We have done so many varieties of family photo shoots and adventures with our sessions, whether it was inside our home or outside, rain or shine, birthday party or a small gathering Event... She's been there for us and captured the most important moments that we still cherish and adore, and those captured moments get better with time. We've become friends with MaryGene due to her beautiful heart and loving personality. Always grateful for how she delivers her work from start to finish. Definitely will recommend to anyone and have been recommending her only!!!"

**-Natalia Makarenko**




>"Having MaryGene as the photographer for our wedding was amazing. She just knew so much about how the wedding day would be and how to arrange the photography to fit into all of the special events that took place. She was able to catch every special moment. She was very thorough and able to work well with everybody. And the photos she took turned out beautiful! She is an expert on capturing what brides, grooms and their families want on the wedding day. I would highly recommend MaryGene to anyone looking for a photographer for their big day or any other special event!"

**-Emma & Jon Montero**




>"We have been enjoying the photography of MaryGene Atwood and Tutus Shots since our youngest daughter was born in September of 2015. She has captured newborn, 3 month, 6 month, 9 month and 1 year cake smash moments. She has also been a special part of my parents 40th wedding anniversary. MaryGene is always flexible, prepared, fun and full of ideas! Her reasonable prices make it easy for a family to record priceless moments without breaking the bank, but without compromising quality. I absolutely LOVE getting all of our photos with a photo release to do what we want with them. I would not hesitate to refer anyone to MaryGene and Tutus Shots."

**-Trisha Sass**
